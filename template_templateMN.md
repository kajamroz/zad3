---
author: Kaja Mróz
title: AURORA
subtitle: Beamer
output: 
    beamer_presentation:
        theme: Warsaw
header-includes: 
    \usepackage{xcolor}
    \usepackage{listings}
---


## O AURORZE

![Aurora](AURORA.jpg)

* **AURORA** to norweska artystka muzyczna.
* Urodziła się w Stavanger w Norwegii.
* Zdobyła rozgłos dzięki unikalnemu głosu, muzyce i stylowi.
* Jest autorką takich utworów jak "Runaway" i "Cure For Me".


## Dyskografia

Albumy studyjne:

1. _All My Demons Greeting Me as a Friend_ (2016)
2. _Infections of a Different Kind (Step 1)_ (2018)
3. _A Different Kind of Human (Step 2)_ (2019)
4. _The Gods We Can Touch_ (2022)

Popularne single:

* "Runaway"
* "Cure For Me"
* "Queendom"


## Muzyka i Wizualizacje

![Cure for me](cfm.jpg)
AURORA znana jest nie tylko z niezwykłej muzyki, ale także z wyjątkowych wizualizacji towarzyszących jej występom.

## Występy na Żywo

![koncert](concert.jpg)
AURORA jest również cenioną artystką koncertową, jej występy na żywo są pełne energii i pasji.

## Występy w Polsce

![Warszawa](warsaw.jpg)
Wielokrotnie występowała w Polsce, chociażby w Warszawskim Palladium lub Progresji.

## AURORA Inspiracją

![Billie Eilish](billie.jpg){height=50% width=50%}
AURORA stała się inspiracją dla wielu młodych artystów na całym świecie. To dzięki niej swoją karierę rozpoczęła chociażby Billie Eilish

## Zakończenie

AURORA to niewątpliwie artystka, z której sztuką warto się zapoznać.
Jej muzyka i występy na żywo zdobywają uznanie na całym świecie.
